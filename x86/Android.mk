ifeq ($(TARGET_ARCH),x86)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := TrichromeLibrary
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := prebuilts/chromium/certs/chromium
LOCAL_MODULE_TARGET_ARCH := x86
LOCAL_SRC_FILES := TrichromeLibrary.apk
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := TrichromeWebView
LOCAL_MODULE_CLASS := APPS
LOCAL_PRODUCT_MODULE := true
LOCAL_CERTIFICATE := prebuilts/chromium/certs/chromium
LOCAL_REQUIRED_MODULES := \
        libwebviewchromium_loader \
        libwebviewchromium_plat_support \
        TrichromeLibrary
LOCAL_OVERRIDES_PACKAGES := webview
LOCAL_MODULE_TARGET_ARCH := x86
LOCAL_SRC_FILES := TrichromeWebView.apk
include $(BUILD_PREBUILT)

endif

